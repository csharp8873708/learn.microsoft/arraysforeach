﻿//Arrays y foreach
//================

//Declarar nueva matriz
// string[] fraudulentOrderIDs = new string[3];

//Asignar valores
// fraudulentOrderIDs[0] = "A123";
// fraudulentOrderIDs[1] = "B456";
// fraudulentOrderIDs[2] = "C789";

//Recuperar valores de una matriz
// Console.WriteLine($"First: {fraudulentOrderIDs[0]}");

//Inicializar una matriz
string[] fraudulentOrderIDs = {"A123", "B456", "C789"};

//Recuperar valores de una matriz
Console.WriteLine($"First: {fraudulentOrderIDs[0]}");

//Obtención del tamaño de una matriz
int length = fraudulentOrderIDs.Length;
Console.WriteLine($"There are {length} fraudulent order to process.");

//Foreach

string[] names = {"Bob", "Conrad", "Grant"};
foreach (string name in names)
{
    Console.WriteLine(name);
}

//Example
int[] inventory = { 200, 450, 700, 175, 250 };
int sum = 0;
int i = 0;
foreach (int items in inventory )
{
    sum += items;
    i++;
    Console.WriteLine($"Bin {i} = {items} (Running total: {sum})");
}
Console.WriteLine($"We have {sum} items in inventory");

//Challenge 1

string[] pedidos = {"B123", "C234", "A345", "C15", "B177", "G3003", "C235", "B179"};
foreach(string pedido in pedidos)
{
    if (pedido.StartsWith("B"))
    {
        Console.WriteLine(pedido);
    }
}

//Challenge 2

int[] ages = { 20, 22, 25, 18, 17, 30, 35, 18, 18, 50, 15, 15};
int suma = 0;
foreach(int age in ages)
{
    suma += age;
}
Console.WriteLine($"La suma de todas las edades es: {suma}");